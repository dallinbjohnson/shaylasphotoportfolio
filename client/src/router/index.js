import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '../store';

import Home from '../views/Home.vue'
import SignUp from '../views/SignUp.vue';
import Login from '../views/Login.vue';
import About from '../views/About.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter(to, from, next) {
      store
        .dispatch('auth/authenticate')
        .then(() => {
          next('/about');
        })
        // eslint-disable-next-line no-unused-vars
        .catch(e => {
          next('/login');
        });
    },
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    beforeEnter(to, from, next) {
      store
        .dispatch('auth/authenticate')
        .then(() => {
          next();
        })
        // eslint-disable-next-line no-unused-vars
        .catch(e => {
          next('/login');
        });
    },
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUp,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
