import Vue from 'vue';
import Vuex from 'vuex';

import { FeathersVuex } from 'feathers-vuex';

import getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions.js'

import users from './services/users'

import auth from './store.auth'

Vue.use(Vuex);
Vue.use(FeathersVuex);

export const getDefaultState = () => {
  return {
    styleTheme: {
      // Main Primary color */
      '--primary-0': '#05B2BA',
      '--primary-1': '#82F2F7',
      '--primary-2': '#59E0E7',
      '--primary-3': '#006064',
      '--primary-4': '#00494C',

      // Main Secondary color (1) */
      '--secondary-1-0': '#D40496',
      '--secondary-1-1': '#FA83D6',
      '--secondary-1-2': '#F05BC3',
      '--secondary-1-3': '#7D0057',
      '--secondary-1-4': '#5F0042',

      // Main Secondary color (2) */
      '--secondary-2-0': '#B6F605',
      '--secondary-2-1': '#DEFE85',
      '--secondary-2-2': '#D2FC60',
      '--secondary-2-3': '#739D00',
      '--secondary-2-4': '#577700',

      // Main Complement color */
      '--complement-0': '#FF7905',
      '--complement-1': '#FFBE86',
      '--complement-2': '#FFAA61',
      '--complement-3': '#A54C00',
      '--complement-4': '#7E3A00',
    },
    loading: false
  }
};

const state = getDefaultState();

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [
    users,
    auth
  ],
});
